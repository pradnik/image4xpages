package cz.pradny.poi;

import com.ibm.xsp.library.AbstractXspLibrary;

public class XspLibrary extends AbstractXspLibrary {

	public static final String LIBRARY_ID = XspLibrary.class.getName();
	
	@Override
	public String getLibraryId() {
		
		return LIBRARY_ID;
	}
	
	@Override
	public String getPluginId() {
		return Activator.PLUGIN_ID;
	}

}
