package cz.pradny.poi;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.context.FacesContext;
import javax.xml.bind.DatatypeConverter;

import lotus.domino.Document;
import lotus.domino.DxlExporter;
import lotus.domino.NotesException;
import lotus.domino.Session;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlToken;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualDrawingProps;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPositiveSize2D;
import org.openxmlformats.schemas.drawingml.x2006.wordprocessingDrawing.CTInline;

import com.ibm.commons.util.StringUtil;

public class ImageExporter {
	private static final Logger logger = Logger.getLogger(ImageExporter.class
			.getName());
	private Document doc;
	private String xml = null;

	public ImageExporter(Document doc) {
		this.doc = doc;
	}

	private String getDocXML() throws NotesException {
		if (xml == null) {
			Session session = getSessionAsSigner();

			DxlExporter dxl = session.createDxlExporter();

			dxl.setConvertNotesBitmapsToGIF(true);
			xml = dxl.exportDxl(doc);
		}

		return xml;
	}

	public void addSignatures(XWPFDocument dxDocument,
			Map<String, String> signatures) {
		try {
			// XWPFDocument dxDocument=(XWPFDocument)
			// getVariableValue("xwpfdocument");

			for (XWPFParagraph paraCurrent : dxDocument.getParagraphs()) {
				processBookmarks2Paragraph(signatures, paraCurrent, dxDocument);
			}

			for (XWPFTable tabCurrent : dxDocument.getTables()) {
				processBookmarks2Table(signatures, tabCurrent, dxDocument);
			}

		} catch (Exception e) {
			logger.log(Level.WARNING, e.toString(), e);
		}
	}

	private void processBookmarks2Paragraph(Map<String, String> signatures,
			XWPFParagraph paraCurrent, XWPFDocument dxDocument)
			throws NotesException, IOException, InvalidFormatException {
		for (XWPFRun runCurrent : paraCurrent.getRuns()) {
			String runText = runCurrent.getText(0);
			if (StringUtil.isNotEmpty(runText)) {
				for (String sig : signatures.keySet()) {
					if (runText.indexOf("<<" + sig + ">>") > -1) {
						runCurrent.setText("",0);
						String outDxl = getDocXML();

						String find1 = "<item name='" + signatures.get(sig)
								+ "'";
						String find2 = "</item>";
						String findpic1 = "<jpeg>";
						String findpic2 = "</jpeg>";

						// System.out.println(outDxl);
						int rtstart = outDxl.indexOf(find1);
						int rtstart2 = outDxl.indexOf(findpic1, rtstart);
						int finish2 = outDxl.indexOf(findpic2, rtstart2);

						String pic = outDxl.substring(
								rtstart2 + findpic1.length(), finish2);

					
						// System.out.println(pic);
						//	BASE64Decoder base64decoder = new BASE64Decoder();
						//byte[] image = base64decoder.decodeBuffer(pic);
						byte[] image = DatatypeConverter.parseBase64Binary(pic);
						
						InputStream is = new ByteArrayInputStream(image);
						// FileOutputStream fo=new FileOutputStream(new
						// File("/tmp/sig.gif"));
						// fo.write(image);
						// fo.close();
						// runCurrent.addPicture(is,
						// XWPFDocument.PICTURE_TYPE_GIF, "signature.gif",
						// Units.toEMU(200), Units.toEMU(200));
						final String blipId = runCurrent.getDocument()
								.addPictureData(is,
										XWPFDocument.PICTURE_TYPE_JPEG);
						final XWPFRun imageRun = runCurrent;
						final XWPFDocument imageDoc = dxDocument;

						try {
							AccessController
									.doPrivileged(new PrivilegedExceptionAction<Void>() {
										public Void run() throws Exception {
											createPicture(
													imageRun,
													blipId,
													imageDoc.getNextPicNameNumber(XWPFDocument.PICTURE_TYPE_JPEG),
													120, 120);
											return null;
										}
									});
						} catch (PrivilegedActionException e) {
							logger.log(Level.WARNING, e.toString(), e);
						}

					}
				}
			}
		}

	}

	private void processBookmarks2Table(Map<String, String> signatures,
			XWPFTable tabCurrent, XWPFDocument dxDocument)
			throws NotesException, InvalidFormatException, IOException {
		for (XWPFTableRow tabRow : tabCurrent.getRows()) {
			for (XWPFTableCell tabCell : tabRow.getTableCells()) {
				for (XWPFParagraph paraCurrent : tabCell.getParagraphs()) {
					processBookmarks2Paragraph(signatures, paraCurrent,
							dxDocument);
				}
			}
		}

	}

	private void createPicture(XWPFRun runCurrent, String blipId, int id,
			int width, int height) {
		final int EMU = 9525;
		width *= EMU;
		height *= EMU;
		// String blipId =
		// getAllPictures().get(id).getPackageRelationship().getId();

		CTInline inline = runCurrent.getCTR().addNewDrawing().addNewInline();

		String picXml = ""
				+ "<a:graphic xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\">"
				+ "   <a:graphicData uri=\"http://schemas.openxmlformats.org/drawingml/2006/picture\">"
				+ "      <pic:pic xmlns:pic=\"http://schemas.openxmlformats.org/drawingml/2006/picture\">"
				+ "         <pic:nvPicPr>" + "            <pic:cNvPr id=\""
				+ id
				+ "\" name=\"Generated\"/>"
				+ "            <pic:cNvPicPr/>"
				+ "         </pic:nvPicPr>"
				+ "         <pic:blipFill>"
				+ "            <a:blip r:embed=\""
				+ blipId
				+ "\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\"/>"
				+ "            <a:stretch>"
				+ "               <a:fillRect/>"
				+ "            </a:stretch>"
				+ "         </pic:blipFill>"
				+ "         <pic:spPr>"
				+ "            <a:xfrm>"
				+ "               <a:off x=\"0\" y=\"0\"/>"
				+ "               <a:ext cx=\""
				+ width
				+ "\" cy=\""
				+ height
				+ "\"/>"
				+ "            </a:xfrm>"
				+ "            <a:prstGeom prst=\"rect\">"
				+ "               <a:avLst/>"
				+ "            </a:prstGeom>"
				+ "         </pic:spPr>"
				+ "      </pic:pic>"
				+ "   </a:graphicData>" + "</a:graphic>";

		// CTGraphicalObjectData graphicData =
		// inline.addNewGraphic().addNewGraphicData();
		XmlToken xmlToken = null;
		try {
			xmlToken = XmlToken.Factory.parse(picXml);
		} catch (XmlException e) {
			logger.log(Level.WARNING, e.toString(), e);
		}
		inline.set(xmlToken);
		// graphicData.set(xmlToken);

		inline.setDistT(0);
		inline.setDistB(0);
		inline.setDistL(0);
		inline.setDistR(0);

		CTPositiveSize2D extent = inline.addNewExtent();
		extent.setCx(width);
		extent.setCy(height);

		CTNonVisualDrawingProps docPr = inline.addNewDocPr();
		docPr.setId(id);
		docPr.setName("Picture " + id);
		docPr.setDescr("Generated");
	}

	private static Object resolveVariable(String variable) {
		return FacesContext.getCurrentInstance().getApplication()
				.getVariableResolver()
				.resolveVariable(FacesContext.getCurrentInstance(), variable);
	}

	private static Session getSessionAsSigner() {
		return (Session) resolveVariable("sessionAsSigner");
	}

}
